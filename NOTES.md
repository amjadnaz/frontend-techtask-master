

### Notes by Amjad

1) Added "./src" as the base url in tsconfig.json so the imports will look cleaner. 

2) Broke down component chunks (based on responsibilty) and moved them to the Components folder. 
Some components can be broken down further and extracted to their own components later if necessary (based on reusabilty)

3) moved some generic functions to helpers.tsx

4) moved constants to constants.js

5) moved queries to queries.js
