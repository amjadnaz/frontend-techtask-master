import { intlNumberFormatValues } from "./constants";

export const formatter = new Intl.NumberFormat(intlNumberFormatValues[0], {
    style: intlNumberFormatValues[1],
    currency: intlNumberFormatValues[2],
  });
  

export function fetchData<Data>(query: string ){

   return fetch('/graphql', {
        method: 'POST',
        headers: {
          'Content-Type':"application/json"
        },
        body: JSON.stringify({query})
      }).then((response: Response) => {
        if(response.status !== 200 && response.status !== 201){
          throw new Error(`Request Failed wtih error code ${response.status}, Error: ${response}` )
        }
        return response.json();
      })
      .then((response: {data: Data} ) => response.data)
      .catch((error: Error) => {
        console.error(error);
        throw new Error();
      });
      
}