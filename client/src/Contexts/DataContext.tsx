import { fetchData } from "helpers";
import { FETCH_PRODUTS } from "queries";
import React, { useContext, createContext } from "react";
import { Category, ChildCategory } from "types";

type DataContext = {
  categories: Category[];
  childCategories: ChildCategory[];
};

export const DataContext = createContext<DataContext>({
  categories: [],
  childCategories: [],
});

export const DataProvider = (props: { children: React.ReactNode }) => {
  const [categories, setCatagories] = React.useState<Category[]>([]);
  const [childCategories, setChildCatagories] = React.useState<ChildCategory[]>(
    []
  );

  React.useEffect(() => {
    fetchData<{ categories: Category[] }>(FETCH_PRODUTS).then(
      ({ categories }) => {
        setCatagories(categories);
        setChildCatagories(categories[0].childrenCategories);
      }
    );
  }, []);

  return (
    <DataContext.Provider value={{ categories, childCategories }} {...props}/>
  );
};

export const useData = (): DataContext => {
  const dataContext = useContext(DataContext);
  if (!dataContext) {
    throw Error("useData ca be use inside a <DataProvider>");
  }
  return dataContext;
};
