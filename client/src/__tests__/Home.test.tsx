import React from "react";
import { render } from "@testing-library/react";
import { Home } from "Components/Home";
import { MemoryRouter } from "react-router-dom";

test("renders the Home", () => {
  const { getByText, getByAltText } = render(
    <MemoryRouter>
      <Home />
    </MemoryRouter>
  );
  const linkElement = getByAltText(/home24/i);
  const sidebarHeading = getByText(/Kategorien/i);
  const loading = getByText(/Loading.../i);
  
  expect(linkElement).toBeInTheDocument();
  expect(sidebarHeading).toBeInTheDocument();
  expect(loading).toBeInTheDocument();
});
