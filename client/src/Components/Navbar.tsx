import React from "react";
import { Link, } from "react-router-dom";



export const Navbar = () => {
    
    return (
    <div className={"header"}>
        <Link to={"/"} className="nav-logo">
            <img data-testid="brand-logo" alt="home24" 
            src="https://cdn1.home24.net/corgi/static/media/home-24-logo.4f73bd13.svg" 
            width="100px" 
            height="35px" 
            className="logo"/>
        </Link>
        <input className={"nav-search"} placeholder={"Search"} />
    </div>
    )
}

