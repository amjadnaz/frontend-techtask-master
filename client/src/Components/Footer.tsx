import React from "react";
import styled from "styled-components";

const StyledFooter = styled.div`
grid-area: footer;
background-color: #f1f1f1;
text-align: center;
`

export const Footer = () => {
    
    return (
     <StyledFooter>
        Alle Preise sind in Euro (€) inkl. gesetzlicher Umsatzsteuer und
        Versandkosten.
      </StyledFooter>
    )
}