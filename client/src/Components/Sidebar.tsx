
import { useData } from "Contexts/DataContext";
import React, {memo} from "react";
import { Link } from "react-router-dom";


export const Sidebar = memo(() => {

  const {childCategories} = useData();

    return (
        <div className={"sidebar"}>
        <h3>Kategorien</h3>
          <ul>
            {childCategories?.map(({ name, urlPath }) =>  (
                <li key={name}>
                  <Link to={`/${urlPath}`}>{name}</Link>
                </li>
              ))}
          </ul>
      </div>
    )
});