import React from "react";
import styled from "styled-components";

export const PageContent = styled.div`
grid-area: content;
grid-column: span 2;
margin-top: 2em;
`