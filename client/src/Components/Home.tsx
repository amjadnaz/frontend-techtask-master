import React from "react";
import { ArticleList } from "Components/ArticleList";
import { Footer } from "Components/Footer";
import { Sidebar } from "Components/Sidebar";
import { Navbar } from "Components/Navbar";
import { DataProvider } from "Contexts/DataContext";
import { PageContent } from "./PageContent";


export const Home = () => {
  return (
    <div className={"page"}>
      <DataProvider>
        <Navbar />
        <Sidebar />
        <PageContent>
          <ArticleList />
        </PageContent>
        <Footer />
      </DataProvider>
    </div>
  );
};
