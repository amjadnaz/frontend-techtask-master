import { useData } from "Contexts/DataContext";
import React, { useMemo } from "react";
import { ArticleCard } from "./ArticleCard";
import { Loader } from "./Loader";

export const ArticleList = () => {
  const { categories } = useData();

  const articles = useMemo(
    () =>
      categories?.map((category) =>
        category.categoryArticles?.articles?.map((article, index) => (
          <ArticleCard article={article} key={`${article.name}-${index}`} />
        ))
      ),
    [categories]
  );

  return (
    <>
      {categories?.length ? (
        <h1>
          {categories[0]?.name}
          <small> ({categories[0]?.articleCount})</small>
        </h1>
      ) : (
        <Loader />
      )}
      <div className={"articles"}>{articles}</div>
    </>
  );
};
