import React, { memo } from "react";
import { formatter } from "../helpers";
import { Article } from "../types";

type ArticleCardState = { 
    article: Article 
}

export const ArticleCard = memo(({ article }:ArticleCardState) => {
    return (
      <div className={"article"}>
        <img src={article.images[0].path} alt={article.name} />
        <div>{article.name}</div>
        <div>{formatter.format(article.prices.regular.value / 100)}</div>
        <section role="button">Add to cart</section>
      </div>
    );
  });