import { Home } from 'Components/Home';
import React from 'react';
import ReactDOM from 'react-dom';
import {  BrowserRouter as Router, Route } from "react-router-dom";
import 'index.css';


ReactDOM.render(
  <React.StrictMode>
    <Router>
      <Route path='/' component={Home} />
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
)
